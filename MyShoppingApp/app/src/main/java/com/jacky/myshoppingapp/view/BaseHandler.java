package com.jacky.myshoppingapp.view;

import android.os.Handler;

import java.lang.ref.WeakReference;

/**
 * Created by Administrator on 2015/10/15.
 */
public class BaseHandler<T> extends Handler {
    protected WeakReference<T> reference;

    public BaseHandler(T obj) {
        reference = new WeakReference<T>(obj);
    }
}
