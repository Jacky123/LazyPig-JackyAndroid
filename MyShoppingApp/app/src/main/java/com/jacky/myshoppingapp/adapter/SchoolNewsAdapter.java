package com.jacky.myshoppingapp.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jacky.myshoppingapp.R;

public class SchoolNewsAdapter extends BaseAdapter {
	private int[] mPic = new int[] { R.drawable.shop_3, R.drawable.shop_3,
			R.drawable.shop_3, R.drawable.shop_3, R.drawable.shop_3,
			R.drawable.shop_3, R.drawable.shop_3, R.drawable.shop_3,
			R.drawable.shop_3, R.drawable.shop_3 };
	private String[] mTitle = new String[] { "我是标题", "我是标题", "我是标题", "我是标题",
			"我是标题", "我是标题", "我是标题", "我是标题", "我是标题", "我是标题" };
	private String[] mContent = new String[] {
			"我是正文我是正文我是正文我是正文我是正文我是正文我是正文我是正文",
			"我是正文我是正文我是正文我是正文我是正文我是正文我是正文我是正文",
			"我是我是正文我是正文我是正文我是正文我是正文我是正文我是正文我是正文正文",
			"我是我是正文我是正文我是正文我是正文我是正文我是正文我是正文我是正文正文",
			"我是正文我是正文我是正文我是正文我是正文我是正文我是正文我是正文我是正文我是正文我是正文我是正文我是正文我是正文我是正文我是正文",
			"我我是正文我是正文我是正文我是正文我是正文我是正文我是正文我是正文",
			"我我是正文我是正文我是正文我是正文我是正文我是正文我是正文我是正文是正文",
			"我是我是正文我是正文我是正文我是正文我是正文我是正文我是正文我是正文正文",
			"我是正我是正文我是正文我是正文我是正文我是正文我是正文我是正文我是正文文",
			"我是正我是正文我是正文我是正文我是正文我是正文我是正文我是正文我是正文文" };
	private Context mcontext;

	public SchoolNewsAdapter(Context context) {
		mcontext = context;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mTitle.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mTitle[position];
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = View
				.inflate(mcontext, R.layout.school_news_item, null);
		ImageView ivPotrait = (ImageView) view
				.findViewById(R.id.iv_school_item);
		TextView tvTitle = (TextView) view
				.findViewById(R.id.tv_school_item_title);
		TextView tvContent = (TextView) view
				.findViewById(R.id.tv_school_item_content);
		tvTitle.setText(mTitle[position]);
		tvContent.setText(mContent[position]);
		ivPotrait.setImageResource(mPic[position]);
		return view;
	}

}
