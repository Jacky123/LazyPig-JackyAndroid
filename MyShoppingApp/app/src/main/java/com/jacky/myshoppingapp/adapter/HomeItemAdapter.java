package com.jacky.myshoppingapp.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jacky.myshoppingapp.R;
import com.jacky.myshoppingapp.bean.HomeBean;

import java.util.List;

public class HomeItemAdapter extends BaseAdapter {
    //    private int[] mPic = new int[]{R.drawable.shop_1, R.drawable.shop_2,
//            R.drawable.shop_3, R.drawable.shop_4, R.drawable.shop_5,
//            R.drawable.shop_6};
//    private String[] mItem = new String[]{"体育用品", "3C数码", "电脑周边", "自行车",
//            "电动车", "更多..."};
    private Context mContext;
    private List<HomeBean> mlist;

    public HomeItemAdapter(Context context, List<HomeBean> list) {
        mContext = context;
        mlist = list;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mlist.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return mlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = View.inflate(mContext, R.layout.home_gridview_item, null);
//        Log.d("LazyPig", "我在这里，，，，");
        ImageView ivItem = (ImageView) convertView.findViewById(R.id.iv_home);
        TextView tvItem = (TextView) convertView.findViewById(R.id.tv_home);

        ivItem.setImageResource(mlist.get(position).getId());
        tvItem.setText(mlist.get(position).getName());

//        Log.d("LazyPig", String.valueOf(position));
        return convertView;
    }

}
