package com.jacky.myshoppingapp.util;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;

/**
 * 一些关于尺寸的工具类
 */
public class SizeUtility {

    public static int getScreenWidth(Activity activity) {
        DisplayMetrics _dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(_dm);
        return _dm.widthPixels;
    }

    public static int getScreenHeitht(Activity activity) {
        DisplayMetrics _dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(_dm);
        return _dm.heightPixels;
    }

    public static int dp2px(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

    public static int px2dp(Context context, float px) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (px / scale + 0.5f);
    }
}
