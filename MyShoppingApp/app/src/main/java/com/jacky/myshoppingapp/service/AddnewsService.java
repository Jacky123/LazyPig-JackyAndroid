package com.jacky.myshoppingapp.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.Toast;

import com.jacky.myshoppingapp.R;
import com.jacky.myshoppingapp.activity.SearchActivity;

public class AddnewsService extends Service {

    private WindowManager mWM;
    private View view;
    private int startX;
    private int startY;
    private WindowManager.LayoutParams params;
    private int winWidth;
    private int winHeight;
    public SharedPreferences pref;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        pref = getSharedPreferences("MyShoppingApp", MODE_PRIVATE);
        /**
         * 自定义一个悬浮窗
         */
        mWM = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics metrics = new DisplayMetrics();
        // 获取屏幕宽高
        mWM.getDefaultDisplay().getMetrics(metrics);
        winWidth = metrics.widthPixels;
        winHeight = metrics.heightPixels;

        params = new WindowManager.LayoutParams();
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        params.width = WindowManager.LayoutParams.WRAP_CONTENT;
        params.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
        params.format = PixelFormat.TRANSLUCENT;
        params.type = WindowManager.LayoutParams.TYPE_TOAST;
        params.gravity = Gravity.LEFT + Gravity.TOP;// 将重心位置设置为左上方,
        // 也就是(0,0)从左上方开始,而不是默认的重心位置
        params.setTitle("Toast");

        view = View.inflate(this, R.layout.news_of_chat_add_service, null);


        // 设置浮窗的位置, 基于左上方的偏移量
        int lastX = pref.getInt("LastX", winWidth - (view.getWidth() / 2));
        int lastY = pref.getInt("LastY", winHeight - (view.getHeight() / 2) - 300);
        params.x = lastX;
        params.y = lastY;

        mWM.addView(view, params);// 将view添加在屏幕上(Window)

        view.setOnTouchListener(new OnTouchListener() {
            long time = 0;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        /**
                         * 实现双击发表说说
                         */
                        if (System.currentTimeMillis() - time > 3000) {
                            Toast.makeText(getApplication(), "双击有惊喜!", Toast.LENGTH_SHORT).show();
                            time = System.currentTimeMillis();
                            // 初始化起点坐标
                            startX = (int) event.getRawX();
                            startY = (int) event.getRawY();

                        } else {
                            //　在服务中没办法直接启动一个新的activity需要加上 addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            Intent intent = new Intent(getApplicationContext(), SearchActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            // 停止服务
                            stopSelf();
//                            Toast.makeText(getApplication(), "正在进入发表说说界面...", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case MotionEvent.ACTION_MOVE:
                        int endX = (int) event.getRawX();
                        int endY = (int) event.getRawY();

                        // 计算移动偏移量
                        int dx = endX - startX;
                        int dy = endY - startY;

                        // 更新浮窗位置
                        params.x += dx;
                        params.y += dy;

                        // 防止坐标偏离屏幕
                        if (params.x < 0) {
                            params.x = 0;
                        }

                        if (params.y < 0) {
                            params.y = 0;
                        }

                        // 防止坐标偏离屏幕
                        if (params.x > winWidth - view.getWidth()) {
                            params.x = winWidth - view.getWidth();
                        }

                        if (params.y > winHeight - view.getHeight()) {
                            params.y = winHeight - view.getHeight();
                        }

                        // System.out.println("x:" + params.x + ";y:" + params.y);

                        mWM.updateViewLayout(view, params);

                        // 重新初始化起点坐标
                        startX = (int) event.getRawX();
                        startY = (int) event.getRawY();
                        break;
                    case MotionEvent.ACTION_UP:
                        /**
                         * 记住上一次的位置
                         */

                        pref.edit().putInt("LastX", params.x).commit();
                        pref.edit().putInt("LastY", params.y).commit();
                        break;

                    default:
                        break;
                }
                return true;
            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mWM != null && view != null) {
            mWM.removeView(view);
        }
    }

}
