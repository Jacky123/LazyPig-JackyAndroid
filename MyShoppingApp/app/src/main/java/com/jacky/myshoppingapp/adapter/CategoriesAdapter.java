package com.jacky.myshoppingapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.jacky.myshoppingapp.R;
import com.jacky.myshoppingapp.activity.DetailsActivity;
import com.jacky.myshoppingapp.activity.OnInnerListener;
import com.jacky.myshoppingapp.bean.Categories;

import java.util.List;

/**
 * 作者：Jacky on 2015/12/29.
 * 首页九宫格商品分类子页面的Adapter
 */
public class CategoriesAdapter extends BaseAdapter {
    private Context mContext;
    private List<Categories> mList;
    private PopupWindow mPopupMenu;
    private View mMenuView;
    private OnInnerListener mListener;

    public CategoriesAdapter(Context context, List<Categories> list, OnInnerListener listener) {
        mContext = context;
        mList = list;
        mListener = listener;
        // initPopupMunu();
    }


    /**
     * 弹窗
     */
/*
    private void initPopupMunu() {
        mMenuView = LayoutInflater.from(mContext).inflate(R.layout.right_menu_layout, null, false);
        mPopupMenu = new PopupWindow(mMenuView, DensityUtil.dip2px(mContext, 90), DensityUtil.dip2px(mContext, 40));
        mPopupMenu.setBackgroundDrawable(new BitmapDrawable());
        TextView _modify = (TextView) mMenuView.findViewById(R.id.modify);
        TextView _delete = (TextView) mMenuView.findViewById(R.id.delete);
        _modify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onModify();
                }
                mPopupMenu.dismiss();
            }
        });
        _delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onDelete();
                }
                mPopupMenu.dismiss();
            }
        });
        mPopupMenu.setOutsideTouchable(true);
        mPopupMenu.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    if (mPopupMenu.isShowing()) {
                        mPopupMenu.dismiss();
                    }
                }
                return false;
            }
        });

    }
*/
    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = View.inflate(mContext, R.layout.gridview_categories_1, null);
            holder = new ViewHolder();

            holder.img = (ImageView) convertView.findViewById(R.id.imageView);
            holder.title = (TextView) convertView.findViewById(R.id.tv_title);
            holder.price = (TextView) convertView.findViewById(R.id.tv_price);
            holder.number = (TextView) convertView.findViewById(R.id.tv_number);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


//        img.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mPopupMenu.setFocusable(true);
//                mPopupMenu.showAsDropDown(img);
//            }
//        });
        holder.img.setImageResource(mList.get(position).getImageView());
        holder.title.setText(mList.get(position).getTitle());

        //  往 TextView 里面加 int 类型的数据时要用 String.valueOf();
        holder.price.setText(String.valueOf(mList.get(position).getPrice()) + " 元");
        holder.number.setText(String.valueOf(mList.get(position).getNumber()) + " 人付款");

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.startActivity(new Intent(mContext, DetailsActivity.class));
            }
        });
        return convertView;
    }

    public class ViewHolder {
        public TextView title;
        public TextView price;
        public TextView number;
        public ImageView img;

    }
}
