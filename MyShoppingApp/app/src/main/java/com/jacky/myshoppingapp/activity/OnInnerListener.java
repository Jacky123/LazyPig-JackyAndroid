package com.jacky.myshoppingapp.activity;

/**
 * 作者：Jacky on 2015/12/30.
 * 点击每个item右侧的按钮弹出的菜单的所有功能函数
 */
public interface OnInnerListener {
    //    修改函数
    public void onModify();

    //删除函数
    public void onDelete();

}
