package com.jacky.myshoppingapp.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.jacky.myshoppingapp.R;
import com.jacky.myshoppingapp.adapter.CategoriesAdapter;
import com.jacky.myshoppingapp.bean.Categories;

import java.util.ArrayList;
import java.util.List;

public class CommonActivity extends Activity {
    private LayoutInflater inflater;
    TextView tv;
    String name;
    private ListView listView;
    private List<Categories> mList = new ArrayList<Categories>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common);


        inflater = LayoutInflater.from(this);
        LinearLayout lin = (LinearLayout) findViewById(R.id.common_layout);
        Intent intent = getIntent();
        name = intent.getStringExtra("Jacky");
        Log.d("LazyPig", name);
        switch (name) {
            case "文具用品":

                initData();
                View view1 = inflater.inflate(R.layout.gridview_item_1, null);

                listView = (ListView) view1.findViewById(R.id.listView);

                CategoriesAdapter adapter = new CategoriesAdapter(this, mList, new OnInnerListener() {
                    @Override
                    public void onModify() {
                        Toast.makeText(CommonActivity.this, "我执行了修改操作！", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onDelete() {
                        Toast.makeText(CommonActivity.this, "我执行了删除操作！", Toast.LENGTH_SHORT).show();
                    }

                });
                listView.setAdapter(adapter);

                lin.removeAllViews();
                lin.addView(view1);
                break;
            case "图书杂志":
                View view2 = inflater.inflate(R.layout.gridview_item_2, null);
                lin.removeAllViews();
                lin.addView(view2);
                break;
            case "3C数码":
                View view3 = inflater.inflate(R.layout.gridview_item_3, null);
                tv = (TextView) view3.findViewById(R.id.textView3);
                tv.setText(name);
                lin.removeAllViews();
                lin.addView(view3);
                break;
            case "自行车":
                View view4 = inflater.inflate(R.layout.gridview_item_4, null);
                tv = (TextView) view4.findViewById(R.id.textView4);
                tv.setText(name);
                lin.removeAllViews();
                lin.addView(view4);
                break;
            case "香香奶茶":
                View view5 = inflater.inflate(R.layout.gridview_item_5, null);
                tv = (TextView) view5.findViewById(R.id.textView5);
                tv.setText(name);
                lin.removeAllViews();
                lin.addView(view5);
                break;
            case "更多...":
                View view6 = inflater.inflate(R.layout.gridview_item_6, null);
                tv = (TextView) view6.findViewById(R.id.textView6);
                tv.setText(name);
                lin.removeAllViews();
                lin.addView(view6);
                break;
        }
    }

    // 初始化文具用品的数据
    public void initData() {
        for (int i = 0; i < 100; i++) {
            Categories ca1 = new Categories("2015韩版休闲男棉衣冬2015韩版休闲男棉衣冬", 125, 999, R.drawable.defaultpic);
            mList.add(ca1);
//            Categories ca2 = new Categories("2015韩版休闲男棉衣冬2015韩版休闲男棉衣冬", 125, 999, R.drawable.defaultpic);
//            mList.add(ca2);
//            Categories ca3 = new Categories("2015韩版休闲男棉衣冬2015韩版休闲男棉衣冬", 125, 999, R.drawable.defaultpic);
//            mList.add(ca3);
//            Categories ca4 = new Categories("2015韩版休闲男棉衣冬2015韩版休闲男棉衣冬", 125, 999, R.drawable.defaultpic);
//            mList.add(ca4);
        }
    }

}
