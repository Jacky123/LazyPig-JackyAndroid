package com.jacky.myshoppingapp.activity;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.jacky.myshoppingapp.R;
import com.jacky.myshoppingapp.service.AddnewsService;

public class NewsOfChatItem extends Fragment {
    private Button Messsage; // 消息
    private MessageOfChatItem messageOfChatItem;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.news_of_chat_fragment, container,
                false);
        Messsage = (Button) view.findViewById(R.id.btn_chat_message);
        Messsage.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                FragmentTransaction transaction = fm.beginTransaction();
                messageOfChatItem = new MessageOfChatItem();
                transaction.replace(R.id.fl_home, messageOfChatItem);
                // 将fragment加入返回栈中，当按下返回键时会回到上一个界面
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        /**
         * 开启一个服务来显示发布动态的按钮
         */
        getActivity().startService(
                new Intent(getActivity(), AddnewsService.class));

        return view;
    }
    /**
     * 停止服务
     */
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getActivity().stopService(
                new Intent(getActivity(), AddnewsService.class));
    }
}
