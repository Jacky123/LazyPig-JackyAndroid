package com.jacky.myshoppingapp.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;

public class ProductDetailViewPager extends ViewPager {


    public ProductDetailViewPager(Context context) {
        super(context);
    }

    public ProductDetailViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

}
