package com.jacky.myshoppingapp.activity;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jacky.myshoppingapp.R;

/**
 * 我的账户的Fragment
 * 
 * @author Administrator
 * 
 */
public class MineItem extends Fragment {
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.mine_fragment, container, false);
		return view;
	}
}
