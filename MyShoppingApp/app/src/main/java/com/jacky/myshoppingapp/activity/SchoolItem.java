package com.jacky.myshoppingapp.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.jacky.myshoppingapp.R;
import com.jacky.myshoppingapp.adapter.SchoolNewsAdapter;

/**
 * 校园活动的Fragment
 * 
 * @author Administrator
 * 
 */
public class SchoolItem extends Fragment {
	private TextView mschoolNews;
	private TextView mschoolBroadcast;
	private TextView mschoolActivity;
	private ViewPager mvieViewPager;
	private int offSet = 0;
	private int bmWidth;
	private ImageView imageView;
	private TranslateAnimation animation;
	private int currentItem;
	private Bitmap cursor;
	private Matrix matrix = new Matrix();
	private ListView mListView;
	private List<View> mList = new ArrayList<View>();
	private SchoolNewsAdapter madapter;

	private View view1, view2, view3, view4;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (view1 == null) {
			view1 = inflater
					.inflate(R.layout.school_fragment, container, false);
		}
		if (view2 == null) {
			view2 = inflater.inflate(R.layout.school_news, container, false);
			mList.add(view2);
		}
		if (view3 == null) {
			view3 = getActivity().getLayoutInflater().inflate(
					R.layout.school_broadcast, null);
			mList.add(view3);
		}
		if (view4 == null) {
			view4 = getActivity().getLayoutInflater().inflate(
					R.layout.school_activity, null);
			mList.add(view4);
		}

		mListView = (ListView) view2.findViewById(R.id.lv_school_news);

		mschoolNews = (TextView) view1.findViewById(R.id.tv_school_news);
		mschoolBroadcast = (TextView) view1
				.findViewById(R.id.tv_school_broadcast);
		mschoolActivity = (TextView) view1
				.findViewById(R.id.tv_school_activity);
		mvieViewPager = (ViewPager) view1.findViewById(R.id.school_viewPager);
		imageView = (ImageView) view1.findViewById(R.id.cursor);
		mListView = (ListView) view2.findViewById(R.id.lv_school_news);
		// 将布局添加进来

		initeCursor();
		setViewPager();
		SchoolviewPagerAdapter adapter = new SchoolviewPagerAdapter();
		mvieViewPager.setAdapter(adapter);
		System.out.println(">>>>>>>>>>>>>>>>>" + "我回来了。。。。。。。。。。");
		madapter = new SchoolNewsAdapter(getActivity());
		mListView.setAdapter(madapter);
		return view1;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	/**
	 * 设置一个viewPager布局
	 */
	private void setViewPager() {
		/**
		 * 设置鼠标监听事件，切换到相应的布局
		 */
		mschoolNews.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mvieViewPager.setCurrentItem(0);

			}
		});
		mschoolBroadcast.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mvieViewPager.setCurrentItem(1);

			}
		});
		mschoolActivity.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mvieViewPager.setCurrentItem(2);

			}
		});
		mvieViewPager
				.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
					@Override
					public void onPageSelected(int arg0) {
						// TODO Auto-generated method stub
						// 当滑动时，顶部的imageView是通过animation缓慢的滑动
						switch (arg0) {
						case 0:
							if (currentItem == 1) {
								animation = new TranslateAnimation(offSet * 2
										+ bmWidth, 0, 0, 0);
							} else if (currentItem == 2) {
								animation = new TranslateAnimation(offSet * 4
										+ 2 * bmWidth, 0, 0, 0);
							}

							break;
						case 1:
							if (currentItem == 0) {
								animation = new TranslateAnimation(0, offSet
										* 2 + bmWidth, 0, 0);
							} else if (currentItem == 2) {
								animation = new TranslateAnimation(4 * offSet
										+ 2 * bmWidth, offSet * 2 + bmWidth, 0,
										0);
							}

							break;
						case 2:
							if (currentItem == 0) {
								animation = new TranslateAnimation(0, offSet
										* 4 + 2 * bmWidth, 0, 0);
							} else if (currentItem == 1) {
								animation = new TranslateAnimation(2 * offSet
										+ bmWidth, offSet * 4 + 2 * bmWidth, 0,
										0);
							}

							break;
						}
						currentItem = arg0;
						animation.setDuration(500);
						animation.setFillAfter(true);
						imageView.startAnimation(animation);

					}

					@Override
					public void onPageScrollStateChanged(int arg0) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onPageScrolled(int arg0, float arg1, int arg2) {
						// TODO Auto-generated method stub

					}
				});
	}

	private void initeCursor() {
		cursor = BitmapFactory
				.decodeResource(getResources(), R.drawable.cursor);
		bmWidth = cursor.getWidth();

		DisplayMetrics dm;
		dm = getResources().getDisplayMetrics();

		offSet = (dm.widthPixels - 3 * bmWidth) / 6;
		matrix.setTranslate(offSet, 0);
		imageView.setImageMatrix(matrix); // 需要iamgeView的scaleType为matrix
		currentItem = 0;
	}

	class SchoolviewPagerAdapter extends PagerAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mList.size();
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			// TODO Auto-generated method stub
			return arg0 == arg1;
		}

		@Override
		public void destroyItem(View view, int position, Object object) // 销毁Item
		{
			((ViewPager) view).removeView(mList.get(position));
		}

		@Override
		public Object instantiateItem(View view, int position) // 实例化Item
		{
			((ViewPager) view).addView(mList.get(position), 0);

			return mList.get(position);
		}

	}
}
