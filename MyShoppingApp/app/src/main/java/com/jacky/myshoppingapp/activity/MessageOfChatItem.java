package com.jacky.myshoppingapp.activity;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.jacky.myshoppingapp.R;

public class MessageOfChatItem extends Fragment {
	private Button News;// 动态
	private NewsOfChatItem newsOfChatItem;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.message_of_chat_fragment,
				container, false);
		News = (Button) view.findViewById(R.id.btn_chat_news);
		News.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				FragmentManager fm = getFragmentManager();
				FragmentTransaction transaction = fm.beginTransaction();
				newsOfChatItem = new NewsOfChatItem();
				transaction.replace(R.id.fl_home, newsOfChatItem);
				transaction.commit();
			}
		});
		return view;
	}
}
