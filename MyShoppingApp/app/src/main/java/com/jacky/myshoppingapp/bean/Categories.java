package com.jacky.myshoppingapp.bean;

/**
 * 作者：Jacky on 2015/12/29.
 * 首页九宫格商品分类子页面的实体类
 */
public class Categories {
    private String title;
    private int price;
    private int number;
    private int imageView;

    public Categories(String title, int price, int number, int imageView) {
        this.title = title;
        this.price = price;
        this.number = number;
        this.imageView = imageView;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getImageView() {
        return imageView;
    }

    public void setImageView(int imageView) {
        this.imageView = imageView;
    }
}
