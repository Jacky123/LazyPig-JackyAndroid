package com.jacky.myshoppingapp.activity;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.jacky.myshoppingapp.R;
import com.jacky.myshoppingapp.adapter.HomeItemAdapter;
import com.jacky.myshoppingapp.bean.HomeBean;
import com.jacky.myshoppingapp.view.HomeViewpager;

import java.util.ArrayList;
import java.util.List;

/**
 * 首页的fragment
 *
 * @author Administrator
 */
public class HomeItem extends Fragment implements OnClickListener {
    private GridView mItemGridView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ImageView search;
    private TextView mNotice;
    private ImageView mMenu;
    private View view;
    private ImageView img_1;
    private ImageView img_2;
    private ImageView img_3;
    private ImageView img_4;
    private List<HomeBean> mList = new ArrayList<HomeBean>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.home_fragment, container, false);
        // 找 ID
        FindId();

        mMenu.setOnClickListener(this);
        search.setOnClickListener(this);
        img_1.setOnClickListener(this);
        img_2.setOnClickListener(this);
        img_3.setOnClickListener(this);
        img_4.setOnClickListener(this);

        new HomeViewpager(getActivity());

        /**
         * 点击跑马灯的文本时进入对应的界面
         */
//        mNotice.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(getActivity(), "点击是进入对应的界面", Toast.LENGTH_SHORT).show();
//            }
//        });

        /**
         * 初始化Gridview 的数据
         */
        initData();
        mItemGridView.setAdapter(new HomeItemAdapter(getActivity(), mList));
        swipeRefreshLayout = (SwipeRefreshLayout) view
                .findViewById(R.id.swipe_container);
        /**
         * 下拉刷新
         */
        // 设置刷新时动画的颜色，可以设置4个
        swipeRefreshLayout.setColorSchemeResources(
                android.R.color.holo_blue_light,
                android.R.color.holo_red_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_green_light);
        swipeRefreshLayout.setOnRefreshListener(new OnRefreshListener() {

            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {

                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, 4000);
            }
        });

        LoadItem();
        return view;
    }

    /**
     * 找资源 ID
     */
    private void FindId() {
        mItemGridView = (GridView) view.findViewById(R.id.gv_item_category);
        search = (ImageView) view.findViewById(R.id.home_item_search);
//        mNotice = (TextView) view.findViewById(R.id.tv_notice);
        mMenu = (ImageView) view.findViewById(R.id.iv_menu);
        img_1 = (ImageView) view.findViewById(R.id.iv_home_1);
        img_2 = (ImageView) view.findViewById(R.id.iv_home_2);
        img_3 = (ImageView) view.findViewById(R.id.iv_home_3);
        img_4 = (ImageView) view.findViewById(R.id.iv_home_4);
    }

    /**
     * 初始化Gridview 的数据
     */
    private List<HomeBean> initData() {
        if (mList.size() == 0) {
            HomeBean Item1 = new HomeBean("文具用品", R.drawable.shop_1);
            mList.add(Item1);
            HomeBean Item2 = new HomeBean("图书杂志", R.drawable.shop_2);
            mList.add(Item2);
            HomeBean Item3 = new HomeBean("3C数码", R.drawable.shop_3);
            mList.add(Item3);
            HomeBean Item4 = new HomeBean("自行车", R.drawable.shop_4);
            mList.add(Item4);
            HomeBean Item5 = new HomeBean("香香奶茶", R.drawable.shop_5);
            mList.add(Item5);
            HomeBean Item6 = new HomeBean("更多...", R.drawable.shop_6);
            mList.add(Item6);
        }
        return mList;
    }

    /**
     * 设置网格布局中点击时跳转的页面
     */
    private void LoadItem() {
        mItemGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Log.d("Jack", mList.get(position).getName());

                //向CommonActivity传入GridView每一项对应的数据，动态加载不同的布局
                Intent intent = new Intent(getActivity(), CommonActivity.class);
                intent.putExtra("Jacky", mList.get(position).getName());
                startActivity(intent);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        Log.d("LazyPig","则是ssss");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_home_1:
                Toast.makeText(getActivity(), ">>>>>>>>>>111111111111", Toast.LENGTH_SHORT).show();
                break;
            case R.id.iv_home_2:
                Toast.makeText(getActivity(), ">>>>>>>>>>222222222", Toast.LENGTH_SHORT).show();
                break;
            case R.id.iv_home_3:
                Toast.makeText(getActivity(), ">>>>>>>>>>333333333", Toast.LENGTH_SHORT).show();
                break;
            case R.id.iv_home_4:
                Toast.makeText(getActivity(), ">>>>>>>>>>444444444444", Toast.LENGTH_SHORT).show();
                break;
            case R.id.home_item_search:
                /**
                 * 实现搜索
                 */
                startActivity(new Intent(getActivity(), SearchActivity.class));
                break;
            case R.id.iv_menu:
                /**
                 * 菜单按钮的单击事件，点击弹出一个popMenu
                 */
                Toast.makeText(getActivity(), "弹出一个popMenu", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
