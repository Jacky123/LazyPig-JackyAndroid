package com.jacky.myshoppingapp.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.jacky.myshoppingapp.R;
import com.jacky.myshoppingapp.util.SizeUtility;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class ProductDetailImagePager extends RelativeLayout {
    //展示图片的VierwPager
    private ViewPager mImagePager;
    //    指示圆点的容器
    private LinearLayout mIndicatorContainer;
    //    整个控件的View
    private RelativeLayout mImagePagerView;
    //    控件包含展示图片的数量
    private int mCount = 0;
    //    当前选中的图片
    private int mCurrnImage = 0;
    //    保存控件展示的图片的集合
    private ArrayList<Bitmap> mBitmaps = new ArrayList<>();
    //    指示圆点的集合
    private ArrayList<ImageView> mIndicators = new ArrayList<>();
    private Context mContext;
    private ArrayList<ImageView> mImages = new ArrayList<>();
    private MyHandler mHandler;
    //    用于循环切换界面的计时器
    private Timer mRecycleTimer;
    //    用于计时器的计时任务
    private TimerTask mRecycleTask;
    //   当图片被点击时回调的接口
    private OnImageClickListener mListener = null;

    public ProductDetailImagePager(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ProductDetailImagePager(Context context) {
        super(context);
        init(context);
    }

    private void init(Context context) {
        mContext = context;
        mImagePagerView = (RelativeLayout) LayoutInflater.from(context).inflate(R.layout.activity_product_detail_image_pager, null);
        mImagePager = (ViewPager) mImagePagerView.findViewById(R.id.product_icon_pager);
        mIndicatorContainer = (LinearLayout) mImagePagerView.findViewById(R.id.incator_container);
        addView(mImagePagerView);
        mHandler = new MyHandler(this);
        initRecycle();
        mImagePager.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    Toast.makeText(mContext, "<<<<<<<<", Toast.LENGTH_SHORT).show();
                    stopRecycle();
                }
                return false;
            }
        });
    }

    public void setImages(ArrayList<Bitmap> images) {
        mBitmaps = images;
        initImages();
        initPager();
        initIndicator();
        recycleImage();
    }

    //    初始化ViewPager中的图片
    private void initImages() {
        mCount = mBitmaps.size();
        for (int i = 0; i < mBitmaps.size(); i++) {
            final int _page = i;
            Bitmap image = mBitmaps.get(i);
            ImageView _image = new ImageView(mContext);
            _image.setImageBitmap(image);
            _image.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.onImageClick(_page);
                    }
                }
            });
            mImages.add(_image);
        }
    }

    //初始化展示图片的ViewPager
    private void initPager() {
        PagerAdapter _adapter = new PagerAdapter() {
            @Override
            public int getCount() {
                return mBitmaps.size();
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                return view == object;
            }

            @Override
            public Object instantiateItem(ViewGroup container, int position) {
                container.addView(mImages.get(position));
                return mImages.get(position);
            }

            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
                container.removeView(mImages.get(position));
            }
        };
        mImagePager.setAdapter(_adapter);
        mImagePager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
//                把之前选中的指示圆点变为未选中状态
                mIndicators.get(mCurrnImage).setImageDrawable(mContext.getResources().getDrawable(R.drawable.pager_indicator_nor));
                mCurrnImage = position;
//                把当前选中的指示圆点设置为选中状态
                mIndicators.get(mCurrnImage).setImageDrawable(mContext.getResources().getDrawable(R.drawable.pager_indicator_sel));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    //    初始化指示小圆点
    private void initIndicator() {
        //            当只有一张图片时隐藏指示圆点
        if (mCount >= 1) {
            ImageView _indicator;
            for (int i = 0; i < mCount; i++) {
                final int _currn = i;
                _indicator = new ImageView(mContext);
                LinearLayout.LayoutParams _lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//                设置指示圆点左侧间距为8dp
                _lp.setMargins(SizeUtility.dp2px(mContext, 8), 0, 0, 0);
                if (i == 0) {
                    _indicator.setImageDrawable(getResources().getDrawable(R.drawable.pager_indicator_sel));
                } else
                    _indicator.setImageDrawable(getResources().getDrawable(R.drawable.pager_indicator_nor));
                _indicator.setLayoutParams(_lp);
                _indicator.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (_currn != mCurrnImage) {
                            mImagePager.setCurrentItem(_currn);
                            restartRecycle();
                        }
                    }
                });
                mIndicatorContainer.addView(_indicator);
                mIndicators.add(_indicator);
            }
        }

    }

    //    初始化定时任务
    private void initRecycle() {
        mRecycleTimer = new Timer();
        mRecycleTask = new TimerTask() {
            @Override
            public void run() {
                int _currn = mCurrnImage;
                _currn++;
//                当选中的图片为最后一张时让下一张选中的图片为第一张
                if (_currn == mCount) {
                    _currn = 0;
                }
                Message _msg = Message.obtain();
                _msg.arg1 = _currn;
                mHandler.sendMessage(_msg);
            }
        };
    }

    private void recycleImage() {
//        每隔3秒切换一次图片
        mRecycleTimer.schedule(mRecycleTask, 3000, 3000);
    }

    public void setOnImageClickListener(OnImageClickListener listener) {
        mListener = listener;
    }

    private void stopRecycle() {
        mRecycleTimer.cancel();
    }


    //重新开始循环 切换图片的任务
    private void restartRecycle() {
       stopRecycle();
        initRecycle();
        recycleImage();
    }

    //用于与工作线程交互的Handler类
    static class MyHandler extends BaseHandler<ProductDetailImagePager> {

        public MyHandler(ProductDetailImagePager obj) {
            super(obj);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            reference.get().mImagePager.setCurrentItem(msg.arg1);
        }
    }

    //    留给使用此控件的Activity回调的接口
    public static interface OnImageClickListener {
        /**
         * 当展示的某一张图片被点击时调用
         *
         * @param currn 点击的图片是第几张
         */
        public void onImageClick(int currn);
    }
}
