package com.jacky.myshoppingapp.bean;

/**
 * Created by Administrator on 2015/12/17.
 */

public class HomeBean {
    private String name;
    private int Id;

    public HomeBean(String name, int id) {
        this.name = name;
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }
}
