package com.jacky.myshoppingapp.activity;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.jacky.myshoppingapp.R;

public class SearchActivity extends Activity {

    private EditText etSearch;
    private Button btnSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_search);
        etSearch = (EditText) findViewById(R.id.search);

        etSearch.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Drawable drawable = etSearch.getCompoundDrawables()[0];
                drawable.isVisible();
                if (drawable == null)
                    return false;

                if (event.getAction() != MotionEvent.ACTION_UP)
                    return false;

                //drawable.getIntrinsicWidth() 获取drawable资源图片呈现的宽度
                if (event.getX() > etSearch.getWidth()- etSearch.getPaddingRight()- drawable.getIntrinsicWidth()) {
                    //进入这表示图片被选中，可以处理相应的逻辑了
                    Toast.makeText(SearchActivity.this, "正在进行，，，", Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });
    }
}
