package com.jacky.myshoppingapp.activity;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.jacky.myshoppingapp.R;
import com.jacky.myshoppingapp.service.AddnewsService;

public class HomeActivity extends Activity implements OnClickListener {
    private TextView Home;
    private TextView Chat;
    private TextView Activity;
    private TextView Mine;

    private HomeItem homeItem;
    private SchoolItem schoolItem;
    private MineItem mineItem;
    private NewsOfChatItem newsOfChatItem;
    private long firstTime = 0;
    private HomeListener mHomeWatcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        // 重写Home键
        registerHomeListener();

        /**
         * 沉浸式状态栏
         */
//        if (VERSION.SDK_INT >= 19) {
////            // 透明状态栏
////            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//			// 透明导航栏
//			getWindow().addFlags(
//					WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
//        }


        Home = (TextView) findViewById(R.id.tv_home);
        Chat = (TextView) findViewById(R.id.tv_chat);
        Activity = (TextView) findViewById(R.id.tv_school);
        Mine = (TextView) findViewById(R.id.tv_mine);

        DefaultFragment();

        Home.setOnClickListener(this);
        Chat.setOnClickListener(this);
        Activity.setOnClickListener(this);
        Mine.setOnClickListener(this);
    }

    // 初始化一开始的布局Fragment
    private void DefaultFragment() {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        homeItem = new HomeItem();
        transaction.replace(R.id.fl_home, homeItem);
        transaction.commit();
    }

    @Override
    public void onClick(View v) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        Home.setEnabled(false);
        switch (v.getId()) {
            case R.id.tv_home:
                Chat.setEnabled(true);
                Activity.setEnabled(true);
                Mine.setEnabled(true);
                if (homeItem == null) {
                    homeItem = new HomeItem();
                }
                transaction.replace(R.id.fl_home, homeItem);
                break;
            case R.id.tv_chat:
                Home.setEnabled(true);
                Chat.setEnabled(false);
                Activity.setEnabled(true);
                Mine.setEnabled(true);
                if (newsOfChatItem == null) {
                    newsOfChatItem = new NewsOfChatItem();

                }
                transaction.replace(R.id.fl_home, newsOfChatItem);

//                System.out.println(">>>>>>>>>>>>>>" + "王尼玛");
                break;
            case R.id.tv_school:
                Home.setEnabled(true);
                Chat.setEnabled(true);
                Activity.setEnabled(false);
                Mine.setEnabled(true);
                if (schoolItem == null) {
                    schoolItem = new SchoolItem();
                }
                transaction.replace(R.id.fl_home, schoolItem);
                break;
            case R.id.tv_mine:
                Home.setEnabled(true);
                Chat.setEnabled(true);
                Activity.setEnabled(true);
                Mine.setEnabled(false);
                if (mineItem == null) {
                    mineItem = new MineItem();
                }
                transaction.replace(R.id.fl_home, mineItem);
                break;

            default:
                break;
        }
        transaction.commit();
    }

    /**
     * 重写回退键，连续按下两次则退出程序
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == event.KEYCODE_BACK) {
            if (System.currentTimeMillis() - firstTime > 2000) {
                Toast.makeText(HomeActivity.this, "再按一次退出程序", Toast.LENGTH_SHORT).show();
                firstTime = System.currentTimeMillis();
            } else {
                finish();
            }
        }
        if (keyCode == event.KEYCODE_HOME) {
//            registerHomeListener();
        }
        return true;
    }

    /**
     * 注册Home键的监听
     */
    private void registerHomeListener() {
        mHomeWatcher = new HomeListener(this);
        mHomeWatcher.setOnHomePressedListener(new HomeListener.OnHomePressedListener() {

            @Override
            public void onHomePressed() {
                //TODO 进行点击Home键的处理
                stopService(new Intent(HomeActivity.this, AddnewsService.class));
//                Toast.makeText(HomeActivity.this, "单击Home键......", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onHomeLongPressed() {

            }
        });
        // 开始监听
        mHomeWatcher.startWatch();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        Log.d("LazyPig", "zhengsssssssssssssssssssssssssssssss");
    }
}
