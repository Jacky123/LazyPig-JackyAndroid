package com.jacky.myshoppingapp.activity;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.jacky.myshoppingapp.R;

public class DetailsActivity extends Activity implements ViewPager.OnPageChangeListener {

    private ViewPager mViewPager;
    private ViewGroup mViewGroup;
    private ImageView[] dots; // 存放小圆点
    private ImageView[] imageViews; // 存放图片
    private int[] imgId; // 图片资源的 ID

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        findId();
        initImgId();
        initDots();
        initImageView();

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    SystemClock.sleep(3000);
                    handler.sendEmptyMessage(0);
                }
            }
        }).start();


        mViewPager.setAdapter(new MyDetailAdapter());
        mViewPager.setOnPageChangeListener(this);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        setImageBackground(position % imageViews.length);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    class MyDetailAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return Integer.MAX_VALUE;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView(imageViews[position % imageViews.length]);
        }

        /**
         * 载入图片进去，用当前的position 除以 图片数组长度取余数是关键
         */
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            container.addView(imageViews[position % imageViews.length], 0);
            return imageViews[position % imageViews.length];
        }
    }

    /**
     * 初始化图片资源
     */
    private void initImageView() {
        imageViews = new ImageView[imgId.length];
        for (int i = 0; i < imageViews.length; i++) {
            ImageView imageView = new ImageView(this);
            imageViews[i] = imageView;
            imageView.setBackgroundResource(imgId[i]);
        }

    }

    /**
     * 初始化小圆点的个数
     */
    private void initDots() {
        dots = new ImageView[imgId.length];

        for (int i = 0; i < dots.length; i++) {
            ImageView imageView = new ImageView(this);
            // 设置小圆点的大小
            imageView.setLayoutParams(new LinearLayout.LayoutParams(10,10));
            dots[i] = imageView;
            if (i == 0) {
                dots[i].setBackgroundResource(R.drawable.pager_indicator_sel);
            } else {
                dots[i].setBackgroundResource(R.drawable.pager_indicator_nor);
            }
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(new ViewGroup.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));
            layoutParams.leftMargin = 5;
            layoutParams.rightMargin = 5;
            mViewGroup.addView(imageView, layoutParams);
        }
    }

    /**
     * 初始化图片资源的Id
     */
    private void initImgId() {
        imgId = new int[]{R.drawable.frag_1, R.drawable.frag_2, R.drawable.frag_3, R.drawable.frag_4, R.drawable.frag_5};
    }

    /**
     * 找资源的 ID
     */
    private void findId() {
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewGroup = (ViewGroup) findViewById(R.id.group);
    }

    private void setImageBackground(int seletItem) {

        for (int i = 0; i < dots.length; i++) {
            if (i == seletItem) {
                dots[i].setImageResource(R.drawable.pager_indicator_sel);
            } else {
                dots[i].setImageResource(R.drawable.pager_indicator_nor);
            }
        }
    }

}
